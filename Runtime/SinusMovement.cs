using UnityEngine;

namespace HiQ.MovementTypes {
    public class SinusMovement : MonoBehaviour {
        [SerializeField]
        public float Amplitude;
        [SerializeField]
        private float Frequency;

        private Vector3 StartPosition;

        private void Start() {
            StartPosition = transform.position;
        }

        private void Update() {
            transform.position = StartPosition + 
                Vector3.up * Mathf.Sin(Time.time * Frequency) * Amplitude;
        }
    }
}
